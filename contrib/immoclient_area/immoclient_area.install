<?php

/**
 * Implements hook_schema().
 */
function immoclient_area_schema() {
  
  $schema = array();
   
  $schema['immoclient_area_default'] = array(
    'description' => 'The base table for the Immo areas',
    'fields' => array(
      'id' => array(
        'description' => 'Primary key of the areas entity',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'wohnflaeche' => array(
        'description' => 'Wohnfläche',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => TRUE, 
        'default' => 0,
        'precision' => 10, 
        'scale' => 2,
      ),
        'nutzflaeche' => array(
        'description' => 'nutzfläche in qm',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE, 
        'precision' => 10, 
        'scale' => 2,
      ),
        'gesamtflaeche' => array(
        'description' => 'Gesamtfläche in qm',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,   
        'precision' => 10, 
        'scale' => 2,
      ),
        'ladenflaeche' => array(
        'description' => 'Ladenfläche in qm',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,     
        'precision' => 10, 
        'scale' => 2,
      ),
        'lagerflaeche' => array(
        'description' => 'lagerfläche in qm',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,          
        'precision' => 10, 
        'scale' => 2,
      ),
        'verkaufsflaeche' => array(
        'description' => 'verkaufsfläche in qm',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'freiflaeche' => array(
        'description' => 'freifläche in qm',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,          
        'precision' => 10, 
        'scale' => 2,
      ),
        'lagerflaeche' => array(
        'description' => 'lagerfläche in qm',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'bueroflaeche' => array(
        'description' => 'Buerofläche in m²',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'bueroteilflaeche' => array(
        'description' => 'Büroteilfläche in m²m',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,          
        'precision' => 10, 
        'scale' => 2,
      ),
        'fensterfront' => array(
        'description' => 'Fensterfront, Länge',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'verwaltungsflaeche' => array(
        'description' => 'verwaltungsfläche in qm',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,          
        'precision' => 10, 
        'scale' => 2,
      ),
        'gastroflaeche' => array(
        'description' => 'verwaltungsfläche in qm',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'grz' => array(
        'description' => 'Grundflächenzahl',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'gfz' => array(
        'description' => 'Geschossflächenzahl',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'bmz' => array(
        'description' => 'Baumassenzahl',
        'type' => 'int',
        'size' => 'normal',//ergibt mit unsigned: 4,294,967,295
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'bgf' => array(
        'description' => 'Bruttogeschossflächenzahl:Summe aller Flächen jedes Geschosses',
        'type' => 'int',
        'size' => 'normal',//ergibt mit unsigned: 4,294,967,295
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'grundstuecksflaeche' => array(
        'description' => 'Grundstuecksflaeche',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'sonstflaeche' => array(
        'description' => 'Sonstige Flaeche',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'anzahl_zimmer' => array(
        'description' => 'Anzahl Zimmer',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'unsigned' => TRUE,
        'precision' => 10, 
        'scale' => 2,
      ),
        'anzahl_kuechen' => array(
        'description' => 'Anzahl Küchen',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Zimmer
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'anzahl_schlafzimmer' => array(
        'description' => 'Anzahl Schlafzimmer',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Schlafzimmer
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'anzahl_badezimmer' => array(
        'description' => 'Anzahl Badezimmer',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Badezimmer
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'anzahl_sep_wc' => array(
        'description' => 'Anzahl separate WC',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 separate WC
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'anzahl_balkone' => array(
        'description' => 'Anzahl Balkone',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Balkone
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'anzahl_terrassen' => array(
        'description' => 'Anzahl Terassen',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Terrassen
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'anzahl_logia' => array(
        'description' => 'Anzahl Logia',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Logias
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'balkon_terrasse_flaeche' => array(
        'description' => 'Größe aller Balkon/Terrasse m²',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'anzahl_wohn_schlafzimmer' => array(
        'description' => 'Anzahl Wohn-Schlafzimmer',
        'type' => 'int',
        'size' => 'tiny',//mit unsigned bis 255 Zimmer
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'gartenflaeche' => array(
        'description' => 'Gartenfläche in m²',
        'type' => 'int',
        'size' => 'normal',//ergibt mit unsigned: 4,294,967,295
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'kellerflaeche' => array(
        'description' => 'Kellerfläche in m²',
        'type' => 'int',
        'size' => 'normal',//ergibt mit unsigned: 4,294,967,295
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'fensterfront_qm' => array(
        'description' => 'Fensterfront in m²',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'grundstuecksfront' => array(
        'description' => 'Grundstücksfront in m zur Strasse hin',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'dachbodenflaeche' => array(
        'description' => 'Dachboden Fläche in m²',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'teilbar_ab' => array(
        'description' => 'ab welcher Mindestgröße kann die Immobilie unterteilt werden (z.B. durch Innenraumausbau), in m²',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'beheizbare_flaeche' => array(
        'description' => 'Beheizbare Fäche in m²',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'anzahl_stellplaetze' => array(
        'description' => 'Gesamtanzahl der Stellplätze, als einfache Alternative zu den stp_.. Elementen',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 0,
      ),
        'plaetze_gastraum' => array(
        'description' => 'Anzahl der Plätze im Gastraum',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Gasträume
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'anzahl_betten' => array(
        'description' => 'Anzahl der Betten',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Betten
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'anzahl_tagungsraeume' => array(
        'description' => 'Anzahl der Tagungsräume',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Tagungsräume
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'vermietbare_flaeche' => array(
        'description' => 'Gesamte vermietbare Fläche (vorrangig bei Renditeobjekten)',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'anzahl_wohneinheiten' => array(
        'description' => 'Anzahl der Wohneinheiten (vorrangig bei Renditeobjekten)',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Tagungsräume
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'anzahl_gewerbeeinheiten' => array(
        'description' => 'Anzahl der Gewerbeeinheiten (vorrangig bei Renditeobjekten)',
        'type' => 'int',
        'size' => 'small',//mit unsigned bis 65,535 Tagungsräume
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'einliegerwohnung' => array(
        'description' => 'Ist eine Einliegerwohnung vorhanden?',
        'type' => 'int',
        'size' => 'tiny',//mit unsigned bis 65,535 Tagungsräume
        'not null' => FALSE,           
        'unsigned' => TRUE,
      ),
        'kubatur' => array(
        'description' => 'Das Volumen eines Bauwerks im m³',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'ausnuetzungsziffer' => array(
        'description' => 'Verhältnis zwischen Parzellenfläche und Bruttogeschossfläche; definiert die maximal zulässige überbauung eines Grundstücks. Hauptsächlich in der Schweiz zu finden. Wert z.B. 0.2  ... 0.6',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'flaechevon' => array(
        'description' => 'Bei Gewerbe Objekten in Miete und Kauf',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
        'flaechebis' => array(
        'description' => 'Bei Gewerbe Objekten in Miete und Kauf',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => FALSE,           
        'precision' => 10, 
        'scale' => 2,
      ),
    ),
    'primary key' => array('id'),
  );
  
  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function immoclient_area_uninstall() {
  // delete the fieldgroups of this bundle
  db_delete('field_group')
      ->condition('bundle', 'immoclient_area')
      ->execute();
  
  // @see https://api.drupal.org/api/drupal/modules!field!field.crud.inc/function/field_delete_field/7
  $fields = immoclient_area_deletable_fields();
  foreach ($fields as $fieldname) {
  field_delete_field($fieldname);
  }
  field_purge_batch(500);
}


/*
 * create an array of fields belonging to immoclient bundle
 */
function immoclient_area_deletable_fields(){
  $options = array('target' => 'slave');
 
  $content_type = 'immoclient_area';

  $sql = "SELECT field_name
          FROM field_config_instance ci
          WHERE ci.bundle = :content_type ";
     
  $result = db_query($sql, array(':content_type' => $content_type,), $options);
  $fields = array();
  foreach ($result->fetchAll() as $key => $field) {
    $fields[] = $field->field_name;
    
  }
  return $fields;
}